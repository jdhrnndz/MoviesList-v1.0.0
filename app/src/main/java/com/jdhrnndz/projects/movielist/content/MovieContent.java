package com.jdhrnndz.projects.movielist.content;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MovieContent {
    public static List<MovieItem> ITEMS = new ArrayList<MovieItem>();

    public static Map<String, MovieItem> ITEM_MAP = new HashMap<String, MovieItem>();

    public static void populate(JSONObject appMetadata){
        JSONArray movies = new JSONArray();
        try {
            if(appMetadata!=null) {
                movies = appMetadata.getJSONArray("movies");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        int length = movies.length();

        Log.d("JSON_DEBUG", length + "");

        if(appMetadata!=null) {
            for (int i = 0; i < length; i++) {
                try {
                    addItem(new MovieItem(movies.getJSONObject(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void addItem(MovieItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(String.valueOf(item.id), item);
    }

    public static class MovieItem{
        private double rating;

        private String title;
        private String overview;
        private String slug;
        private String backdropUrl;
        private String coverUrl;

        private int id;
        private int year;

        public MovieItem(JSONObject jsonObject) throws JSONException {
            this.rating = jsonObject.getDouble("rating");

            this.title = jsonObject.getString("title");
            this.overview = jsonObject.getString("overview");
            this.slug = jsonObject.getString("slug");
            this.backdropUrl = "https://dl.dropboxusercontent.com/u/5624850/movielist/images/" + this. slug + "-backdrop.jpg";
            this.coverUrl = "https://dl.dropboxusercontent.com/u/5624850/movielist/images/" + this. slug + "-cover.jpg";

            this.id = jsonObject.getInt("id");
            this.year = jsonObject.getInt("year");
        }

        public String getTitle() {
            return this.title;
        }

        public int getYear() {
            return this.year;
        }

        public String getOverview(){
            return this.overview;
        }

        public int getId(){
            return this.id;
        }

        public double getRating(){
            return this.rating;
        }

        public String getBackdropUrl() {
            return backdropUrl;
        }

        public String getCoverUrl() {
            return coverUrl;
        }
    }
}
