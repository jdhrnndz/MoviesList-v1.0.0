package com.jdhrnndz.projects.movielist;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.jdhrnndz.projects.movielist.content.MovieContent;

/**
 * A fragment representing a single Movie detail screen.
 * This fragment is either contained in a {@link MovieListActivity}
 * in two-pane mode (on tablets) or a {@link MovieDetailActivity}
 * on handsets.
 */
public class MovieDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy content this fragment is presenting.
     */
    private MovieContent.MovieItem mItem;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MovieDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mItem = MovieContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_movie_detail, container, false);
        View header = rootView.findViewById(R.id.header);

        if (mItem != null) {
            ((TextView) header.findViewById(R.id.title)).setText(mItem.getTitle());
            ((TextView) header.findViewById(R.id.year_released)).setText(String.valueOf(mItem.getYear()));
            RatingBar rb = ((RatingBar) header.findViewById(R.id.rating));
            rb.setRating((float) mItem.getRating());
            ((TextView) rootView.findViewById(R.id.overview)).setText(mItem.getOverview());
            new DownloadImageTask(((ImageView) rootView.findViewById(R.id.backdrop))).execute(mItem.getBackdropUrl());
            new DownloadImageTask(((ImageView) header.findViewById(R.id.cover))).execute(mItem.getCoverUrl());
        }

        return rootView;
    }
}

