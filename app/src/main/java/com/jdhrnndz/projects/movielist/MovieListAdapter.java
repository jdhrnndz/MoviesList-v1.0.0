package com.jdhrnndz.projects.movielist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jdhrnndz.projects.movielist.content.MovieContent.MovieItem;

import java.util.ArrayList;

class MovieListAdapter extends ArrayAdapter<MovieItem> {

    private ArrayList<MovieItem> movies;

    public MovieListAdapter(Context context, int resource, ArrayList<MovieItem> movies) {
        super(context, resource, movies);
        this.movies = movies;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.movie_item, null);
        }

        MovieItem i = movies.get(position);

        if (i != null) {
            TextView title = (TextView) v.findViewById(R.id.title);
            TextView year = (TextView) v.findViewById(R.id.year_released);
            ImageView backdrop = (ImageView) v.findViewById(R.id.backdrop);

            if (title != null) {
                title.setText(i.getTitle());
            }
            if (year != null) {
                year.setText(String.valueOf(i.getYear()));
            }
            if (backdrop != null) {
                new DownloadImageTask(backdrop).execute(i.getBackdropUrl());
            }
        }

        // the view must be returned to our activity
        return v;

    }
}
